package br.com.order.core.useCase

import br.com.order.core.domain.Checkout
import br.com.order.core.domain.request.CheckoutRequest

interface IUseCaseCheckout {

    fun checkout(checkoutRequest: CheckoutRequest): Checkout

    fun findByNumberCheckout(numberCheckout: String): Checkout
}