package br.com.order.core.useCase

import br.com.order.core.domain.Product

fun interface IProductUseCase {

    fun findByCode(code: String): Product

}