package br.com.order.core.useCase.impl

import br.com.order.core.domain.Client
import br.com.order.core.domain.exception.NotFoundException
import br.com.order.core.domain.request.ClientOrderRequest
import br.com.order.infra.extensions.toClient
import br.com.order.core.repositoryService.ClientRepositoryService
import br.com.order.core.useCase.IClientUseCase
import org.springframework.stereotype.Service

@Service
class ClientUseCase(
        private val repository: ClientRepositoryService
): IClientUseCase {

    override fun findByDoc(cpf: String): Client {
        return repository.findByDoc(cpf)
                ?: throw NotFoundException("Client not Found")
    }

    override fun create(client: Client): Client {
        return repository.create(client)
    }

    override fun findByEmail(email: String): Client? {
        return repository.findByEmail(email)
    }

    override fun findOrCreate(clientOrderRequest: ClientOrderRequest?): Client {
        return clientOrderRequest
                ?.let { findByEmail(it.email!!) }
                ?: create(clientOrderRequest?.toClient()!!)

    }
}