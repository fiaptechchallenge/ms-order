package br.com.order.core.useCase

import br.com.order.infra.enums.OrderStatus
import br.com.order.core.domain.Order
import br.com.order.core.domain.request.OrderStoreRequest
import br.com.order.core.domain.response.OrderQueueResponse


interface IOrderUseCase {

    fun createStore(request: OrderStoreRequest): Order

    fun createWeb(orderStoreRequest: OrderStoreRequest): Order

    fun findByNumberOrder(numberOrder: String): Order

    fun findByStatus(status: OrderStatus): MutableList<Order>?

    fun findOrderByFlow(): OrderQueueResponse

    fun update(order: Order): Order

    fun updateStatus(numberOrder: String, status: OrderStatus): Order
}