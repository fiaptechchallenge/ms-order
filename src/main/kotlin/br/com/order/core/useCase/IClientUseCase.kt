package br.com.order.core.useCase

import br.com.order.core.domain.Client
import br.com.order.core.domain.request.ClientOrderRequest

interface IClientUseCase {

    fun findByDoc(cpf: String): Client
    fun create(client: Client): Client
    fun findByEmail(email: String): Client?
    fun findOrCreate(clientOrderRequest: ClientOrderRequest?): Client

}