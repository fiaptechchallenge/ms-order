package br.com.order.core.useCase.impl

import br.com.order.core.domain.Product
import br.com.order.core.domain.exception.NotFoundException
import br.com.order.core.useCase.IProductUseCase
import br.com.order.infra.api.port.ProductApi
import br.com.order.infra.extensions.toDomain
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class ProductUseCase(
    private val productApi: ProductApi
): IProductUseCase {
    val logger = LoggerFactory.getLogger(this::class.java)

    override fun findByCode(code: String): Product {
        logger.info("Find Product by code: {}", code)
        return productApi.findCode(code)
            .body
            ?.toDomain()
            ?:throw NotFoundException("Product ($code) not found.")
    }

}