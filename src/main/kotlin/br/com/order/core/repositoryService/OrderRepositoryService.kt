package br.com.order.core.repositoryService

import br.com.order.infra.enums.OrderStatus
import br.com.order.core.domain.Order

interface OrderRepositoryService {

    fun create(order: Order): Order

    fun findByNumberOrder(numberOrder: String): Order

    fun findByStatus(status: OrderStatus): MutableList<Order>

    fun findAllOrderPending(status: MutableList<OrderStatus>): MutableList<Order>
}