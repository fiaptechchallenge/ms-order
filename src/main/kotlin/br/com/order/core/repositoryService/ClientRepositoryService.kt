package br.com.order.core.repositoryService

import br.com.order.core.domain.Client

interface ClientRepositoryService {

    fun create(client: Client): Client
    fun findByDoc(cpf: String): Client?
    fun findByEmail(email: String): Client?
    fun list(): MutableList<Client>
}