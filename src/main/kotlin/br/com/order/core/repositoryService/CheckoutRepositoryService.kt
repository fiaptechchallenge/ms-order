package br.com.order.core.repositoryService

import br.com.order.core.domain.Checkout

interface CheckoutRepositoryService {

    fun create(checkout: Checkout): Checkout

    fun findByNumerCheckout(numberCheckout: String): Checkout?

    fun existNumbercheckout(number: String): Boolean

}