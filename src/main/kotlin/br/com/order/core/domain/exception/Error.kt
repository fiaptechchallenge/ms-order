package br.com.order.core.domain.exception

data class Error(
        val message: String,
        val httpCode: Int,
        val httpError: String
)
