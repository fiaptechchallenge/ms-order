package br.com.order.core.domain.request

import br.com.order.infra.enums.TypeDelivery
import br.com.order.core.domain.Address

data class OrderWebRequest(
    val emailClient: String? = null,
    val deliveryAddress : Address? = null,
    val items: MutableList<OrderItemRequest>,
    val typeDelivery: TypeDelivery
)



