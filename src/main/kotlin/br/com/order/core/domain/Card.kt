package br.com.order.core.domain

import br.com.order.infra.enums.Brand

data class Card(
        val nameCard: String,
        val number: String,
        val dateValidate: String,
        val code: Number,
        val brand: Brand,
        val document: String
)
