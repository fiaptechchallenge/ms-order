package br.com.order.core.domain.response

import br.com.order.infra.enums.OrderStatus

data class CheckoutResponse(
        val numberCheckout: String,
        val status: OrderStatus
)
