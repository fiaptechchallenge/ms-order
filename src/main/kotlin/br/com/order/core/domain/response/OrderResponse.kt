package br.com.order.core.domain.response

import br.com.order.infra.enums.OrderStatus
import java.time.LocalDateTime

data class OrderResponse(
        val numberOrder: String,
        val total: Double,
        val status: OrderStatus,
        val dateCreate: LocalDateTime
)
