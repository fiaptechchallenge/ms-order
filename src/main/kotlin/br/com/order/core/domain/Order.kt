package br.com.order.core.domain

import br.com.order.infra.enums.OrderStatus
import br.com.order.infra.enums.TypeDelivery
import org.bson.types.ObjectId
import java.time.LocalDateTime

data class Order (
        val id: ObjectId? = null,
        var numberOrder: String,
        val client: Client? = null,
        val deliveryAddress : Address? = null,
        val items: MutableList<OrderItem>,
        val total: Double,
        var status: OrderStatus,
        val typeDelivery: TypeDelivery,
        val createAt: LocalDateTime
)