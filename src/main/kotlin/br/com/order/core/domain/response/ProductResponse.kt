package br.com.order.core.domain.response

import br.com.order.infra.enums.ProductCategory
import java.time.LocalDateTime

data class ProductResponse(
        val name: String,
        val code: String,
        val price: Double,
        val quantity:  Number,
        val category: ProductCategory,
        val dateValidate: LocalDateTime
)
