package br.com.order.core.domain.exception

class NotFoundException(
        override val message: String?,
        override val cause: Throwable? = null
): Exception(message, cause) {

}