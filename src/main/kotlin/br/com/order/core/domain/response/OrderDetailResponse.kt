package br.com.order.core.domain.response

import br.com.order.infra.enums.OrderStatus
import br.com.order.infra.enums.TypeDelivery
import java.time.LocalDateTime

data class OrderDetailResponse(
        val numberOrder: String,
        val client: ClientResponse? = null,
        val total: Double,
        val status: OrderStatus,
        val items: MutableList<OrderItemResponse>,
        val typeDelivery: TypeDelivery,
        val createAt: LocalDateTime
)
