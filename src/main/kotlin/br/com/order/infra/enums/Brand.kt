package br.com.order.infra.enums

enum class Brand {

    VISA,
    MASTER,
    ELO
}