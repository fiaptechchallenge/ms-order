package br.com.order.infra.enums

enum class ProductCategory {
    SNACK,
    DRINK,
    ACCOMPANIMENT,
    COMBO
}