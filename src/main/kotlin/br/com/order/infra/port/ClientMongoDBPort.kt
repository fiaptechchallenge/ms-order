package br.com.order.infra.port

import br.com.order.infra.model.ClientDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface ClientMongoDBPort: MongoRepository<ClientDocument, ObjectId> {

    fun findByCpf(cpf: String): ClientDocument?

    fun findByEmail(email: String): ClientDocument?
}