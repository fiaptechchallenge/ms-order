package br.com.order.infra.port

import br.com.order.infra.enums.OrderStatus
import br.com.order.infra.model.OrderDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository

interface OrderMongoDBPort: MongoRepository<OrderDocument, ObjectId> {

    fun findByNumberOrder(numberOrder: String): OrderDocument?

    fun findByStatus(status: MutableList<OrderStatus>): MutableList<OrderDocument>?
    fun findByStatusNotIn(status: MutableList<OrderStatus>): MutableList<OrderDocument>?


}