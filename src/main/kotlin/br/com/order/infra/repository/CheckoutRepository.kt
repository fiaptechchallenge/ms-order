package br.com.order.infra.repository

import br.com.order.core.domain.Checkout
import br.com.order.infra.extensions.toDocument
import br.com.order.infra.extensions.toDomain
import br.com.order.core.repositoryService.CheckoutRepositoryService
import br.com.order.infra.port.CheckoutMongoDBPort
import org.springframework.stereotype.Repository

@Repository
class CheckoutRepository(
        private val checkoutMongoDBPort: CheckoutMongoDBPort
): CheckoutRepositoryService {


    override fun create(checkout: Checkout): Checkout {
        return checkoutMongoDBPort.save(checkout.toDocument()).toDomain()
    }

    override fun findByNumerCheckout(numberCheckout: String): Checkout? {
        return checkoutMongoDBPort.findByNumberCheckout(numberCheckout)?.toDomain()
    }

    override fun existNumbercheckout(number: String): Boolean {
        return checkoutMongoDBPort.existsByOrderNumberOrder(number)
    }
}