package br.com.order.infra.extensions

import br.com.order.infra.model.ClientDocument
import br.com.order.core.domain.Client
import br.com.order.core.domain.request.ClientOrderRequest
import br.com.order.core.domain.request.ClientRequest
import br.com.order.core.domain.response.ClientResponse

fun Client.toDocument(): ClientDocument =
        ClientDocument(
                id = id,
                name = name,
                email = email,
                cpf = cpf
        )

fun ClientDocument.toDomain(): Client =
        Client(
                id = id,
                name = name,
                email = email,
                cpf = cpf
        )

fun ClientRequest.toDomain(): Client =
        Client(
                name = name,
                email = email,
                cpf = cpf
        )

fun Client.toResponse(): ClientResponse =
        ClientResponse(
                name = name,
                email = email,
                cpf = cpf
        )

fun ClientOrderRequest.toClient(): Client =
        Client(
                name = name,
                email = email,
                cpf = cpf
        )
