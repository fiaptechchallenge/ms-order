package br.com.order.infra.extensions

import br.com.order.infra.model.AddressDocument
import br.com.order.core.domain.Address

fun Address.toDocument(): AddressDocument =
        AddressDocument(
                street = street,
                number = number,
                complement = complement,
                neighborhood = neighborhood,
                city = city,
                zipCode = zipCode
        )

fun AddressDocument.toDomain(): Address =
        Address(
                street = street,
                number = number,
                complement = complement,
                neighborhood = neighborhood,
                city = city,
                zipCode = zipCode
        )