package br.com.order.infra.extensions

import br.com.order.core.domain.Product
import br.com.order.core.domain.request.ProductRequest
import br.com.order.core.domain.response.ProductResponse
import br.com.order.infra.api.enums.ProductCategoryApi
import br.com.order.infra.api.request.ProductApiRequest
import br.com.order.infra.api.response.ProductApiResponse
import br.com.order.infra.enums.ProductCategory

fun ProductRequest.toDomain(): Product =
        Product(
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )

fun Product.toResponse(): ProductResponse =
        ProductResponse(
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category,
                dateValidate = dateValidate
        )
fun ProductApiResponse.toDomain(): Product =
        Product(
                name = name,
                code = code,
                price = price,
                quantity = quantity,
                category = category.toDomain(),
                dateValidate = dateValidate
        )

fun ProductCategoryApi.toDomain(): ProductCategory =
        ProductCategory.valueOf(this.name)