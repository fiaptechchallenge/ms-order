package br.com.order.infra.extensions

import br.com.order.infra.model.OrderDocument
import br.com.order.infra.model.OrderItemDocument
import br.com.order.core.domain.Order
import br.com.order.core.domain.OrderItem
import br.com.order.core.domain.response.OrderDetailResponse
import br.com.order.core.domain.response.OrderItemResponse
import br.com.order.core.domain.response.OrderResponse

fun Order.toDocument(): OrderDocument =
        OrderDocument(
                id = id,
                numberOrder = numberOrder,
                client = client?.toDocument(),
                deliveryAddress = deliveryAddress?.toDocument(),
                items = items.map { it.toDocument() }.toMutableList(),
                total = total,
                status = status,
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderDocument.toDomain(): Order =
        Order(
                id = id,
                numberOrder = numberOrder,
                client = client?.toDomain(),
                deliveryAddress = deliveryAddress?.toDomain(),
                items = items.map { it.toDomain() }.toMutableList(),
                total = total,
                status = status,
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderItem.toDocument(): OrderItemDocument =
        OrderItemDocument(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

fun OrderItemDocument.toDomain(): OrderItem =
        OrderItem(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

fun Order.toResponse(): OrderResponse =
        OrderResponse(
                numberOrder = numberOrder,
                total = total,
                status = status,
                dateCreate = createAt
        )

fun Order.toOrderDetailResponse(): OrderDetailResponse =
        OrderDetailResponse(
                numberOrder = numberOrder,
                client = client?.toResponse(),
                total = total,
                status = status,
                items = items.map { it.toOrderItemResponse() }.toMutableList(),
                typeDelivery = typeDelivery,
                createAt = createAt
        )

fun OrderItem.toOrderItemResponse(): OrderItemResponse =
        OrderItemResponse(
                name = name,
                codeProduct = codeProduct,
                quantity = quantity,
                price = price
        )

