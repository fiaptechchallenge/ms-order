package br.com.order.infra.api.port

import br.com.order.infra.api.request.ProductApiRequest
import br.com.order.infra.api.response.ProductApiResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(value = "ms-manager-produce", url = "\${feign-api.ms-manager-produce.uri}")
interface ProductApi {

    @GetMapping("/product/code/{code}")
    fun findCode(@PathVariable code: String): ResponseEntity<ProductApiResponse>

    @PutMapping("/product/{code}")
    fun update(@PathVariable code: String, @RequestBody productRequest: ProductApiRequest): ResponseEntity<Unit>

}