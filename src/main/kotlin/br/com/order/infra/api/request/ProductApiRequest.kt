package br.com.order.infra.api.request

import br.com.order.infra.api.enums.ProductCategoryApi
import java.time.LocalDateTime

data class ProductApiRequest(

    val name: String,

    val code: String,

    val price: Double,

    val quantity:  Number,

    val category: ProductCategoryApi,

    val dateValidate: LocalDateTime
)
