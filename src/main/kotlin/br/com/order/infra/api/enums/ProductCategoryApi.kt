package br.com.order.infra.api.enums

enum class ProductCategoryApi {
    SNACK,
    DRINK,
    ACCOMPANIMENT,
    COMBO
}
